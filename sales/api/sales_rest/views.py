from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from sales_rest.models import Salesperson, Customer, Sale, AutomobileVO
import json
from .encoders import (
    SalespersonDetailEncoder,
    CustomerDetailEncoder,
    SalesDetailEncoder,
)


@require_http_methods(['GET','POST'])
def api_list_salespeople(request):
    if request.method == 'GET':
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False
        )


@require_http_methods(['GET','DELETE'])
def api_show_salespeople(request, pk):
    if request.method == 'GET':
        salesperson = Salesperson.objects.get(employee_id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )
    elif request.method == 'DELETE':
        count, _ = Salesperson.objects.filter(employee_id=pk).delete()
        return JsonResponse(
            {'deleted':count > 0},
        )


@require_http_methods(['GET','POST'])
def api_list_customer(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {'message': 'could not create customer'}
            )
            response.status_code = 400
            return response


@require_http_methods(['GET','DELETE'])
def api_show_customer(request, id):
    if request.method == 'GET':
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method == 'DELETE':
        try:
            count, _ = Customer.objects.filter(id=id).delete()

            return JsonResponse(
                {'deleted':count > 0},
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {'message': 'Does not exist'}
            )
            response.status_code = 404
            return response


@require_http_methods(['GET','POST'])
def api_list_sales(request):
    if request.method == 'GET':
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            vin = content["automobile"]
            auto = AutomobileVO.objects.get(vin=vin)
            setattr(auto, 'sold', True)
            auto.save()
            content["automobile"] = auto
            id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=id)
            content["salesperson"] = salesperson
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
            print(content)
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SalesDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create sale"}
            )
            response.status_code = 400
            return response


@require_http_methods(['GET','DELETE'])
def api_show_sales(request, pk):
    if request.method == 'GET':
        try:
            sales = Sale.objects.get(id=pk)
            return JsonResponse(
                sales,
                encoder=SalesDetailEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"}
            )
            response.status_code = 404
            return response
    elif request.method == 'DELETE':
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesDetailEncoder,
                safe=False
            )
        except sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"}
            )
            response.status_code = 404
            return response
