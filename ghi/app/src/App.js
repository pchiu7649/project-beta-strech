import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonsList from './SalespersonsList';
import SalespersonsForm from './SalespersonsForm';
import CustomerList from './CustomerList';
import NewCustomerForm from './NewCustomerForm';
import SalesList from './SalesList';
import NewSalesForm from './NewSalesForm';
import SalesHistory from './SalesHistory';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ManufacturersList from './ManufacturersList';
import NewManufacturer from './NewManufacturer';
import AutomobileList from './AutomobileList';
import NewAutomobile from './NewAutomobile';
import NewModel from './NewModelForm';
import ModelList from './ModelList';
import ServiceHistory from './ServiceHistory';
import './index.css'

function App({technicians}) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />}>
            <Route path="salespersons" element={<SalespersonsList />} />
            <Route path="new-salespersons" element={<SalespersonsForm />} />
            <Route path="customers" element={<CustomerList />} />
            <Route path="new-customers" element={<NewCustomerForm />} />
            <Route path="sales" element={<SalesList />} />
            <Route path="new-sales" element={<NewSalesForm />} />
            <Route path="sales-history" element={<SalesHistory />} />
            <Route path="appointments" element={<AppointmentList />} />
            <Route path="new-appointment" element={<AppointmentForm />} />
            <Route path="technicians" element={<TechnicianList technicians={technicians} />} />
            <Route path="new-technicians" element={<TechnicianForm />} />
            <Route path="service-history" element={<ServiceHistory />} />
            <Route path="models" element={<ModelList />} />
            <Route path="new-model" element={<NewModel />} />
            <Route path="manufacturers" element={<ManufacturersList />} />
            <Route path="new-manufacturer" element={<NewManufacturer />} />
            <Route path="automobiles" element={<AutomobileList />} />
            <Route path="new-automobile" element={<NewAutomobile />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
