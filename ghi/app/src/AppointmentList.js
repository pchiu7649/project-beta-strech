import { useState, useEffect } from 'react';


const fetchConfig = {
    method:"PUT",
    header: {
        'Content-Type': 'application/json',
    },
};


function AppointmentList() {
    const [appointments, setAppointments] = useState([]);

    useEffect( () => {
        const fetchAppointments = async () => {
            try {
                const url = `http://localhost:8080/api/appointments/`;
                const res = await fetch(url);
                const data = await res.json();
                console.log({res, data})
                setAppointments(data.appointments);
            } catch(err) {
                console.log(err);
            }
        }
        fetchAppointments()
    }, [])
    const onClick = async (id) => {
        try {
            const url = `http://localhost:8080/api/appointments/${id}/cancel`;
            await fetch(url, fetchConfig);
        } catch(err) {
            console.log(err);
        }
    }
    const onClick2 = async (id) => {
        try {
            const url = `http://localhost:8080/api/appointments/${id}/finish`;
            await fetch(url, fetchConfig);
        } catch(err) {
            console.log(err);
        }
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Date</th>
                        <th>Technician Name</th>
                        <th>Employee ID</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments ? appointments.map(appointment => {
                        return(
                            <tr key={appointment.employee_id}>
                                <td>{appointment.customer}</td>
                                <td>{appointment.vin}</td>
                                <td>{appointment.vip}</td>
                                <td>{appointment.date_time}</td>
                                <td>{appointment.technician.first_name}, {appointment.technician.last_name}</td>
                                <td>{appointment.technician.employee_id}</td>
                                <td className='buttonWrapper'>
                                    <button className="cancelButton" onClick={() => onClick(appointment.id)}>Cancel</button>
                                    <button className="finishButton" onClick={() => onClick2(appointment.id)}>Finish</button>
                                </td>

                            </tr>
                        );
                    }): <tr><td>Nothing's here!!!!</td></tr>}
                </tbody>
            </table>
        </>
    )
}

export default AppointmentList;
