import { useState, useEffect, useMemo } from 'react';


function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [query, setQuery] = useState('');

    const fetchAppointmentsData = async () => {
        const url = `http://localhost:8080/api/appointments/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }
    useEffect(() => {
        fetchAppointmentsData();
    }, []);

    const handleQueryChange = (event) => {
        setQuery(event.target.value);
      }
    
      const filteredItems = useMemo(() => {
        return appointments.filter(appointment => appointment.vin.includes(query))
      }, [query]);
    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Service History</h1>
                            <form>
                                <div className="mb-3">
                                    <input placeholder="Search by VIN's" onChange={handleQueryChange}/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Customer</th>
                                        <th>VIN</th>
                                        <th>VIP</th>
                                        <th>Reason</th>
                                        <th>Date</th>
                                        <th>Technician Name</th>
                                        <th>Employee ID</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {filteredItems.length ? filteredItems.map(appointment => {
                                        console.log(appointment.reason)
                                        return (
                                            <tr key={appointment.id}>
                                                <td>{appointment.customer}</td>
                                                <td>{appointment.vin}</td>
                                                <td>{appointment.vip}</td>
                                                <td>{appointment.reason}</td>
                                                <td>{appointment.date_time}</td>
                                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                                <td>{appointment.technician.employee_id}</td>
                                                <td>{appointment.status}</td>
                                            </tr>
                                        );
                                    }) : <tr><td>There's nothing here</td></tr>}
                                </tbody>
                            </table>
        </>
    )
}

export default ServiceHistory
