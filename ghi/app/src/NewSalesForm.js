import React, { useEffect, useState } from 'react';
import SalespersonsList from './SalespersonsList';

function NewSalesForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salespersons, setSalespersons] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [vin, setVin] = useState('');
    const [salesperson, setSalesperson] = useState('')
    const [customer, setCustomer] = useState('')
    const [price, setPrice] = useState('')


    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value)
    }
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value)
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value)
    }
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = vin;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const salesUrl = `http://localhost:8090/api/sales/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const automobileUrl = `http://localhost:8100/api/automobiles/${vin}/`;
            const soldAuto = {"sold": true}
            const fetchAutoConfig = {
                method: 'put',
                body: JSON.stringify(soldAuto),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const soldResponse = await fetch(automobileUrl, fetchAutoConfig)
        setVin('');
        setSalesperson('');
        setCustomer('');
        setPrice('');
        }
    }
    const fetchAutomobileData = async () => {
        const url = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles([...data.autos])
        }
    }
    const fetchSalespersonData = async () => {
        const url = `http://localhost:8090/api/salespeople/`
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setSalespersons([...data.salespeople])
        }
    }
    const fetchCustomersData = async () => {
        const url = `http://localhost:8090/api/customers/`
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setCustomers([...data.customers])
        }
    }

    useEffect(() => {
        fetchAutomobileData();
        fetchSalespersonData();
        fetchCustomersData();
    }, []);

    return(
        <div className="containter">
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
                <div className="mb-3">
                    <select onChange={handleVinChange} value={vin} required name="vin" id="vin" className="form-select">
                        <option defaultValue="">Choose an automobile VIN</option>
                        {automobiles.map(automobile => {
                            if (automobile.sold == false) {
                                return (
                                    <option key={automobile.id} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                )
                            };
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                        <option defaultValue="">Choose a salesperson</option>
                        {salespersons.map(salesperson => {
                            return (
                                <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                        <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                            <option defaultValue="">Choose a customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>
                                        {customer.first_name} {customer.last_name}
                                    </option>
                                )
                            })}
                        </select>
                </div>
                <div className="form-floating mb-3">
                        <input onChange={handlePriceChange} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control"/>
                        <label htmlFor="price">Price</label>
                    </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
        </div>
        </div>
    )
} export default NewSalesForm