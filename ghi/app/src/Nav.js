import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/salespersons">
                Salespersons
              </NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/new-salespersons">
                New Salesperson
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/customers">
                Customers
              </NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/new-customers">
                New Customer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/sales">
                Sales
              </NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/new-sales">
                New Sale
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/sales-history">
                Sales History
              </NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/service-history">
                Service History
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/appointments">
                Appointments
              </NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/new-appointment">
                New Appointment
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/technicians">
                Technicians
              </NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/new-technicians">
                New Technician
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/manufacturers">
                Manufacturers
              </NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/new-manufacturer">
                New Manufacturer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/automobiles">
                Automobiles
              </NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/new-automobile">
                New Automobile
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/models">
                Models
              </NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/new-model">
                New Model
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
