import { useState, useEffect } from 'react';

function CustomerList() {
    const [customers, setCustomers] = useState([]);
    const fetchData = async () => {
        const url = `http://localhost:8090/api/customers/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers([...data.customers])
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Phone number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers ? customers.map(customer => {
                        return(
                            <tr key={customer.id}>
                                <td>{customer.first_name}</td>
                                <td>{customer.last_name}</td>
                                <td>{customer.phone_number}</td>
                                <td>{customer.address}</td>
                            </tr>
                        )
                    }):<tr><td>There's nothing here</td></tr>}
                </tbody>
            </table>
        </>
    )
} export default CustomerList