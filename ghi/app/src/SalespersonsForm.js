import React, {useEffect, useState} from 'react';

function SalespersonForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        const url = `http://localhost:8090/api/salespeople/`;
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json',
            },
        };
        const salespersonResponse = await fetch(url, fetchConfig);
        if (salespersonResponse.ok) {
            setFirstName('');
            setLastName('');
        }
    }
    return (
        <div className="container">
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFirstNameChange} value={firstName} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control"/>
                    <label htmlFor="first_name">First name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleLastNameChange} value={lastName} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control"/>
                    <label htmlFor="last_name">Last name</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
        </div>
        </div>
    )

} export default SalespersonForm