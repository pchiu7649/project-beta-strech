import React, {useEffect, useState} from 'react';

function ModelForm() {

    const [manufacturers, setManufacturers] = useState([])

    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const fetchManufacturersData = async () => {
        const url = `http://localhost:8100/api/manufacturers/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }



    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer = manufacturer;

        const manufacturerUrl = `http://localhost:8100/api/models/`;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'applications/json'
            },
        };
        const modelResponse = await fetch(manufacturerUrl, fetchConfig);
        if (modelResponse.ok) {
            setName('');
            setPictureUrl('');
            setManufacturer('');
        }
    }

    useEffect(() => {
        fetchManufacturersData();
    }, []);

    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a New Model</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleUrlChange} value={pictureUrl} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="color">Model Pic</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                            <option defaultValue="bin">Choose a Manufacturer</option>
                            {manufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                        {manufacturer.name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
    )
} export default ModelForm
