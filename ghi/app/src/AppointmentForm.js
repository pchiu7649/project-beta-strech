import React, {useEffect, useState} from 'react';

function AppointmentForm(){
    
    const [technicians, setTechnicians] = useState([]);

    const [dateTime, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState('');

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians([...data.technicians]);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.date_time = dateTime;
        data.reason = reason;
        data.vin = vin;
        data.customer = customer;
        data.technician = technician;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json',
            },
        };
        const appointmentResponse = await fetch(appointmentUrl, fetchConfig);
        if (appointmentResponse.ok) {
            setDateTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new appointment</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleDateTimeChange} value={dateTime} placeholder="Date" required type="date" name="date-time" id="date-time" className="form-control"/>
                        <label htmlFor="name">Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
                        <label htmlFor="color">Reason</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control"/>
                        <label htmlFor="color">VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"/>
                        <label htmlFor="color">Customer</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className="form-select">
                            <option defaultValue="technician">Choose a technician</option>
                            {technicians.map(technician => {
                                return (
                                    <option key={technician.employee_id} value={technician.employee_id}>
                                        {technician.first_name}, {technician.last_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
    )
} export default AppointmentForm
