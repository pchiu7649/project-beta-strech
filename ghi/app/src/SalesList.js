import { useState, useEffect } from 'react';

function SalesList() {

    const [automobiles, setAutomobiles] = useState([]);
    const [sales, setSales] = useState([])
    const fetchAutomobileData = async () => {
        const url = `http://localhost:8100/api/automobiles/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles([...data.autos]);
        }
    }
    const fetchData = async () => {
        const url = `http://localhost:8090/api/sales/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales([...data.sales]);
        }
    }
    useEffect(() => {
        fetchAutomobileData();
        fetchData();
    }, []);
    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson employee id</th>
                        <th>Salesperson name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales ? sales.map(sale => {
                        return(
                            <tr key={sale.id}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        )
                    }):<tr><td>There's nothing here</td></tr>}
                </tbody>
            </table>
        </>
    )
} export default SalesList