from common.json import ModelEncoder

from .models import AutomobileVO, Technician, Appointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id"]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["date_time", "customer"]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time", "reason", "status", "vin", "vip", "customer", "id", "technician"
    ]
    encoders = {
        "technician": TechnicianListEncoder()
    }
