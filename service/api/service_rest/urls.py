from django.urls import path

from .views import api_appointments_cancel, api_appointments_finish, api_list_technicians, api_show_technicians, api_list_appointments, api_show_appointments

urlpatterns = [
    path(
        "technicians/", 
        api_list_technicians, 
        name="api_list_technicians"
    ),
    path(
        "technicians/<int:pk>/",
        api_show_technicians,
        name="api_show_technicians",
    ),
    path(
        "appointments/",
        api_list_appointments,
        name="api_list_appointments"
    ),
    path(
        "appointments/<int:id>/",
        api_show_appointments,
        name="api_show_appointments",
    ),
    path(
        "appointments/<int:pk>/cancel",
        api_appointments_cancel,
        name="api_show_appointments",
    ),
    path(
        "appointments/<int:pk>/finish",
        api_appointments_finish,
        name="api_show_appointments",
    ),

]
