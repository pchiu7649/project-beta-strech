# CarCar

Team:

* Joshua James - Service microservice
* Peter Chiu - Sale microservice

## Design

## Service microservice

Created AutomobileVO for the use of cross referencing the vin from Inventories automobile vin with appointment vin to see if they're the same and if so give the Appointment VIP. Technician was created to keep track of the names and their id's. Appointment was used to setup an appointment with the date reason, who the customer is and technician foreign key to connect the appointment with a technician who will finish the project. Finally status for the us to know if the customer canceled the appointment or if the technician completed it.

## Sales microservice

The sales microservice has four models:
AutomobileVO for the use of retrieving the VIN and 'sold' status from the inventory.
Salesperson model is used to attach to a sale when a sale is made.
Customer is used to keep track of who is purchasing each vehicle.
Sale keeps track of price and is made up of three foreign keys, automobile, salesperson, and customer.
